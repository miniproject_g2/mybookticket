import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TicketBookingSystem bookingSystem = new TicketBookingSystem();

        // Add some movies to the system
        bookingSystem.addMovie("Movie 1", "2023-08-20", 10);
        bookingSystem.addMovie("Movie 2", "2023-08-25", 15);

        while (true) {
            System.out.println("Select an option:");
            System.out.println("1. Book a seat");
            System.out.println("2. View booking history");
            System.out.println("3. Exit");
            int option = scanner.nextInt();

            if (option == 1) {
                scanner.nextLine(); // Consume the newline character

                System.out.println("Select a movie:");

                for (int i = 0; i < bookingSystem.numBookedTickets; i++) {
                    Ticket ticket = bookingSystem.tickets[i];
                    System.out.println((i + 1) + ". " + ticket.getEventName() + " on " + ticket.getEventDate());
                }

                int movieChoice = scanner.nextInt();
                if (movieChoice >= 1 && movieChoice <= bookingSystem.numBookedTickets) {
                    Ticket chosenTicket = bookingSystem.tickets[movieChoice - 1];

                    System.out.println("Available seats for " + chosenTicket.getEventName() + " on " + chosenTicket.getEventDate() + ":");
                    for (int seat = 0; seat < chosenTicket.getNumSeats(); seat++) {
                        if (chosenTicket.isSeatAvailable(seat)) {
                            System.out.print(seat + " ");
                        }
                    }
                    System.out.println();

                    System.out.print("Enter the seat number: ");
                    int seatNumber = scanner.nextInt();

                    bookingSystem.bookTicket(chosenTicket.getEventName(), chosenTicket.getEventDate(), seatNumber);
                } else {
                    System.out.println("Invalid movie choice.");
                }
            } else if (option == 2) {
                bookingSystem.displayBookingHistory();
            } else if (option == 3) {
                System.out.println("Exiting the ticket booking system.");
                break;
            } else {
                System.out.println("Invalid option. Please try again.");
            }
        }

        scanner.close();
    }
}

