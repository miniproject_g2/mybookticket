public class Ticket {
    private String eventName;
    private String eventDate;
    private int numSeats;
    private boolean[] seatAvailability;

    public Ticket(String eventName, String eventDate, int numSeats) {
        this.eventName = eventName;
        this.eventDate = eventDate;
        this.numSeats = numSeats;
        this.seatAvailability = new boolean[numSeats];
        for (int i = 0; i < numSeats; i++) {
            seatAvailability[i] = true;
        }
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public int getNumSeats() {
        return numSeats;
    }

    public boolean isSeatAvailable(int seatNumber) {
        return seatNumber >= 0 && seatNumber < numSeats && seatAvailability[seatNumber];
    }

    public void bookSeat(int seatNumber) {
        if (isSeatAvailable(seatNumber)) {
            seatAvailability[seatNumber] = false;
        }
    }
}
