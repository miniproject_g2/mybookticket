public class TicketBookingSystem {
    Ticket[] tickets;
    int numBookedTickets;
    BookingHistory history;
    private static final int MAX_CAPACITY = 10;

    public TicketBookingSystem() {
        tickets = new Ticket[MAX_CAPACITY];
        numBookedTickets = 0;
        history = new BookingHistory(MAX_CAPACITY);
    }

    public void addMovie(String eventName, String eventDate, int numSeats) {
        Ticket ticket = new Ticket(eventName, eventDate, numSeats);
        tickets[numBookedTickets] = ticket;
        numBookedTickets++;
    }

    public void bookTicket(String eventName, String eventDate, int seatNumber) {
        for (int i = 0; i < numBookedTickets; i++) {
            Ticket ticket = tickets[i];
            if (ticket.getEventName().equals(eventName) && ticket.getEventDate().equals(eventDate)) {
                if (ticket.isSeatAvailable(seatNumber)) {
                    ticket.bookSeat(seatNumber);
                    history.addTicket(ticket);
                    System.out.println("Successfully booked seat " + seatNumber + " for " + eventName + " on " + eventDate + ".");
                } else {
                    System.out.println("Seat " + seatNumber + " is not available.");
                }
                return;
            }
        }
        System.out.println("Movie not found.");
    }

    public void displayBookingHistory() {
        history.displayHistory();
    }
}
