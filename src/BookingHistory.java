public class BookingHistory  {
    private Ticket[] bookedTickets;
    private int numBookedTickets;

    public BookingHistory(int maxCapacity) {
        bookedTickets = new Ticket[maxCapacity];
        numBookedTickets = 0;
    }

    public void addTicket(Ticket ticket) {
        if (numBookedTickets < bookedTickets.length) {
            bookedTickets[numBookedTickets] = ticket;
            numBookedTickets++;
        }
    }

    public void displayHistory() {
        System.out.println("Booking History:");
        for (int i = 0; i < numBookedTickets; i++) {
            Ticket ticket = bookedTickets[i];
            System.out.println(ticket.getEventName() + " on " + ticket.getEventDate());
        }
    }
}
